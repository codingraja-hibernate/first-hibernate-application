package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Customer;

public class SaveCustomer {
	
	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure();
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		//Create customer object
		Customer customer = new Customer("Coding", "RAJA", "info@codingraja.com", "9878254878");
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(customer);
		transaction.commit();
		session.close();
		
		System.out.println("Customer has been saved successfully!");

	}

}
